from django.contrib import admin
from datetime import date

from .models import Post


class PostMesesFilter(admin.SimpleListFilter):
    #nombre con el que veremos el filtro en la lista de filtros
    title = 'filtrar meses año corriente'

    #nombre que veremos en el parámentro de nuestra URL al aplicar el filtro
    parameter_name = 'meses'

    def lookups(self, request, model_admin):
        #tuplas 1 valor parámetro url, 2 valor el que veremos en el input
        return (
            ('1', 'enero'),
            ('2', 'febrero'),
            ('3', 'marzo'),
            ('4', 'abril'),
            ('5', 'mayo'),
            ('6', 'junio'),
            ('7', 'julio'),
            ('8', 'agosto'),
            ('9', 'septiembre'),
            ('10', 'octubre'),
            ('11', 'noviembre'),
            ('12', 'diciembre'),
        )

    def queryset(self, request, queryset):
        #devuelve un queryset según el valor del parámetro recibido
        if self.value():
            year = date.today().year
            param = "%02d" % int(self.value())
            print(year)
            print(param)
            if param:
                return Post.objects.filter(status='P', date_creation__year=year, date_creation__month=param)
